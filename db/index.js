const mysql = require('mysql')

// 创建数据连接池
var pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: '3306',
  database: 'sqleditor'
})

module.exports = pool