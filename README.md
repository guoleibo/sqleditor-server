# SQLEditor Server

#### 介绍
本项目 Vue SQLEditor 在线 SQL 编辑器项目的后端示例代码，基于 Node + Exopress 实现，数据库使用 mysql。

Vue SQLEditor 编辑器的前端项目地址：[https://gitee.com/web1024cn/vue-sqleditor](http://https://gitee.com/web1024cn/vue-sqleditor)

#### 安装教程

1.  git clone https://gitee.com/web1024cn/sqleditor-server.git
2.  npm install
3.  npm start

项目的访问地址为 http://127.0.0.1:3000，本项目没有提供页面，均为 RESTful api 接口

#### 接口说明


- 执行SQL语句 http://127.0.0.1:3000/ext （post，params:{sql:''}）
- 查询所有表名 http://127.0.0.1:3000/query/tables (get)
- 查询指定表下的字段 http://127.0.0.1:3000/query/field （get）


#### 代码介绍


- 基于 express@4.16.1 开发
- 使用 mysql@2.18.1 模块连接 MySQL 数据库
- 使用 mysql 模块提供的 pool 实例实现数据连接池

> 注意：项目使用了 nodemon 管理热部署，需要先在本地安装 nodemon ！本项目使用的 nodemon 为全局安装的

